import { read_previous_events } from '../src/logic';

beforeEach(jest.restoreAllMocks);

const fs = require('fs').promises;
const filesystem = require('fs');
const process = require('process');

describe('read_previous_event', () => {
  [null, undefined].forEach((path) =>
    it(`it should return an empty array if path is ${path}`, () => {
      expect.assertions(1);
      read_previous_events(path).then((res) => expect(res).toEqual([]));
    }),
  );

  it(`it should return an empty array if path does not exist`, () => {
    const exist = jest
      .spyOn(filesystem, 'existsSync')
      .mockImplementation(() => false);
    expect.assertions(3);
    return read_previous_events().then((res) => {
      expect(res).toEqual([]);
      expect(exist).toHaveBeenCalledTimes(1);
      expect(exist).toHaveBeenCalledWith(undefined);
    });
  });

  it(`it should return an empty array if parse says so`, () => {
    jest.spyOn(filesystem, 'existsSync').mockImplementation(() => true);
    const mock = jest
      .spyOn(fs, 'readFile')
      .mockImplementation(() => Promise.resolve('[]'));
    expect.assertions(3);
    const path = 'directory';
    return read_previous_events(path).then((res) => {
      expect(mock).toHaveBeenCalledTimes(1);
      expect(mock).toHaveBeenCalledWith(path, { encoding: 'utf-8' });
      expect(res).toEqual([]);
    });
  });

  it(`it should end process if file cannot be read`, () => {
    jest.spyOn(filesystem, 'existsSync').mockImplementation(() => true);
    const exit = jest.spyOn(process, 'exit').mockImplementation(() => true);
    const error = jest.spyOn(console, 'error').mockImplementation(() => true);
    const mock = jest
      .spyOn(fs, 'readFile')
      .mockImplementation(() => Promise.reject('ERROR'));
    expect.assertions(2);
    const path = 'directory';
    return read_previous_events(path).then((res) => {
      expect(exit).toHaveBeenCalledWith(1);
      expect(error).toHaveBeenCalledWith('ERROR');
    });
  });

  it(`it should end process if JSON cannot be parsed`, () => {
    jest.spyOn(filesystem, 'existsSync').mockImplementation(() => true);
    const exit = jest.spyOn(process, 'exit').mockImplementation(() => true);
    const error = jest.spyOn(console, 'error').mockImplementation(() => true);
    const mock = jest
      .spyOn(fs, 'readFile')
      .mockImplementation(() => Promise.resolve('ERROR'));
    expect.assertions(2);
    const path = 'directory';
    return read_previous_events(path).then((res) => {
      expect(exit).toHaveBeenCalledWith(1);
      expect(error).toHaveBeenCalledTimes(1);
    });
  });
});
