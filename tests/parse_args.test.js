import { parse_args } from '../src/logic';

const process = require('process');
const process_mock = jest.spyOn(process, 'exit').mockImplementation(() => true);

describe('test parse args', () => {
  it.only('parses help options', () => {
    const res = parse_args(['livesentralen']);
    expect(res.page_id).toEqual(
      'https://www.facebook.com/livesentralen/events/',
    );
  });
  it.only('parses help options', () => {
    const res = parse_args(['"livesentralen"']);
    expect(res.page_id).toEqual(
      'https://www.facebook.com/livesentralen/events/',
    );
  });
  ['-?', '--help', '-h'].forEach((param) => {
    it('parses help options', () => {
      const res = parse_args([param]);
      expect(process_mock).toHaveBeenCalledWith(1);
    });
  });

  it('parses event option', () => {
    const res = parse_args(['--events=events.json']);
    expect(res.events).toEqual('events.json');
  });

  it('parses event option', () => {
    const res = parse_args(['--events="events.json"']);
    expect(res.events).toEqual('events.json');
  });

  it('sets events to null if no option is passed', () => {
    const res = parse_args(['']);
    expect(res.events).toEqual(null);
  });

  it('sets output to null if no option is passed', () => {
    const res = parse_args(['']);
    expect(res.output).toEqual(null);
  });

  it('passes output if it is set with --output', () => {
    const res = parse_args(['--output=jacobi']);
    expect(res.output).toEqual('jacobi');
  });

  it('passes output if it is set with --output', () => {
    const res = parse_args(['--output="jacobi"']);
    expect(res.output).toEqual('jacobi');
  });

  it('passes output if it is set with -o', () => {
    const res = parse_args(['-o', 'jacobi']);
    expect(res.output).toEqual('jacobi');
  });

  it('passes output if it is set with -o', () => {
    const res = parse_args(['-o', '"jacobi"']);
    expect(res.output).toEqual('jacobi');
  });

  it('parses skip upcoming events option', () => {
    const res = parse_args(['--skip-upcoming-events']);
    expect(res.get_upcoming_events).toEqual(false);
  });

  it('sets the correct default value for getting upcoming events', () => {
    const res = parse_args([]);
    expect(res.get_upcoming_events).toEqual(true);
  });

  it('sets the correct default value for getting upcoming events', () => {
    const res = parse_args([]);
    expect(res.get_past_events).toEqual(false);
  });

  it('parses skip upcoming events option', () => {
    const res = parse_args(['--past-events']);
    expect(res.get_past_events).toEqual(true);
  });

  it('parses no-headless option to true', () => {
    const res = parse_args(['']);
    expect(res.headless).toBe(true);
  });

  it('parses no-headless option to false', () => {
    const res = parse_args(['--no-headless']);
    expect(res.headless).toBe(false);
  });
});
