FROM node:14.2-alpine
MAINTAINER Jørgen Sverre Lien Sellæg <jorgen@guut.org>

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
  PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser \
  PATH="$PATH:/app/bin"

RUN apk add --no-cache \
  chromium \
  bash \
  nss \
  freetype \
  freetype-dev \
  harfbuzz \
  ca-certificates \
  ttf-freefont \
  && mkdir /app \
  && chown node:node /app

WORKDIR "/app"

USER node

COPY . /app

RUN yarn

CMD ["scrape"]

