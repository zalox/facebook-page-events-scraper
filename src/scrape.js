const {
  by_date,
  event_date_to_date_obj,
  parse_args,
  read_previous_events,
  to_unique_events,
  write_events,
} = require('./logic');
const { get_page_events } = require('./browse');

const options = parse_args(process.argv.slice(2));

(async () => {
  let events = [];
  let prev_events = [];

  try {
    events = await get_page_events(options);
    prev_events = await read_previous_events(options.events);
    events = events
      .reduce(to_unique_events, prev_events)
      .map(event_date_to_date_obj)
      .sort(by_date);
  } catch (e) {
    console.error(e);
  }

  if (options.output === null) {
    console.log(JSON.stringify(events));
    process.exit();
  }

  try {
    await write_events(options.output, events);
    process.exit();
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
