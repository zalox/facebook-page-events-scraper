# Facebook page events scraper
Now you can get JSON representation of all events hosted by a Facebook page.

## Disclaimer
Please note that terms and conditions apply from Facebook and this script should
only be used if written consent is provided by Facebook.

## Dependencies

```txt
bash
node >= v10
yarn
google-chrome / chromium
```

NOTE: You might want to set environment variables to use an existing chrome 
      install. (Voluntary)
``` sh
export PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
export PUPPETEER_EXECUTABLE_PATH=/bin/google-chrome
```

## Usage
```sh
./bin/scrape PlanetRammstein
./bin/scrape --skip-upcoming-events --past-events PlanetRammstein
```

### Usage with docker
```sh
docker build --tag facebook-page-events-scraper .
docker run \
    --rm \
    --cap-add SYS_ADMIN \
    facebook-page-events-scraper \
    scrape PlanetRammstein
```
